package coinpurse;

import java.util.ResourceBundle;

public class MainFactory {
	
	public static void main(String[] args){
		
		MoneyFactory factory = MoneyFactory.getInstance();
		
		Valuable m = factory.createMoney(5);
		System.out.println(m.toString());
		Valuable m2 = factory.createMoney("1000.0");
		System.out.println(m2.toString());
		Valuable m3 = factory.createMoney(0.05);
		System.out.println(m3.toString());
//		
		factory.setMoneyFactory(new MalayMoneyFactory());
		factory = MoneyFactory.getInstance();
		Valuable n = factory.createMoney(5);
		System.out.println(n.toString());
		Valuable n2 = factory.createMoney("1000.0");
		System.out.println(n2.toString());
//		Valuable n3 = factory.createMoney(0.05);
//		System.out.println(n3.toString());
		
	}

}
