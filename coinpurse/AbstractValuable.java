package coinpurse;

/**
 * This class is an abstract class for money.
 * @author Salilthip 5710546640
 *
 */
public abstract class AbstractValuable implements Valuable{

	/**
	 * Compare value of money.
	 * @param valuable is a object that want to compare.
	 * @return -1, if there isn't valuable.
	 * 		   1, if the new value is more than the other.
	 *         -1, if the new value is less than the other.
	 *         0, if the value is different of all case.
	 */
	public int compareTo(Valuable valuable){
		if(valuable==null) return -1;
		else if(this.getValue()>valuable.getValue()) return 1;
		else if(this.getValue()<valuable.getValue()) return -1;
		else return 0;
	}
	
	
    /**
     *  Compare between two object.
     *  @param obj is another object
     *  @return false, if there isn't object.

     *   false,if class of two object are different class.
     *   true,if two object have the same value.

     */
    public boolean equals(Object obj){
    	
    	if (obj == null)
    		return false;
		if ( obj.getClass() != this.getClass() )
			return false;
		
		Valuable other = (Valuable) obj;
		
		if ( getValue()== other.getValue() )
			return true;
		
		return false;
    }
	
}
