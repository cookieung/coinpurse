package coinpurse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import com.sun.xml.internal.fastinfoset.util.ContiguousCharArrayArray;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveStrategy;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A coin purse contains coins. You can insert coins, withdraw money, check the
 * balance, and check if the purse is full. When you withdraw money, the coin
 * purse decides which coins to remove.
 * 
 * @author Salilthip 5710546640
 **/
public class Purse extends Observable{

	private List<Valuable> money;
	
	private WithdrawStrategy strategy;

	private int capacity;

/**
 * Construct a Purse
 * @param capacity is how many of number coin can collect in this purse
 **/
	public Purse(int capacity) {
		money = new ArrayList<Valuable>();
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of coins in the purse. This is the number of
	 * coins, not their value.
	 * 
	 * @return the number of coins in the purse
	 **/
	public int count() {
		return money.size();
	}

	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 **/
	public double getBalance() {
		double total = 0;
		for (int i = 0; i < money.size(); i++)
			total += money.get(i).getValue();
		return total;
	}

	/**
	 * Return the capacity of the coin purse.
	 * 
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * 
	 * @return true if purse is full.
	 **/
	public boolean isFull() {
		if (count() == capacity)
			return true;
		else
			return false;
	}

	/**
	 * Insert a coin into the purse. The coin is only inserted if the purse has
	 * space for it and the coin has positive value. No worthless coins!
	 * 
	 * @param coin
	 *            is a Coin object to insert into purse
	 * @return true if coin inserted, false if can't insert
	 **/
	public boolean insert(Valuable key) {
		if (!isFull()) {
			money.add(key);
			super.setChanged();
			super.notifyObservers(this);
			return true;
		} else
			return false;
	}

	/**
	 * Withdraw the requested amount of money. Return an array of Coins
	 * withdrawn from purse, or return null if cannot withdraw the amount
	 * requested.
	 * 
	 * @param amount
	 *            is the amount to withdraw
	 * @return array of Coin objects for money withdrawn, or null if cannot
	 *         withdraw requested amount.
	 **/
	public Valuable[] withdraw(double amount) {
		if(amount==0) return null;
		Valuable[] result = strategy.withdraw(amount, money);
		
		if(result==null) result = strategy.withdraw(amount, money);
		
		for(int i=0; i<result.length;i++)
			money.remove(result[i]);
		
		super.setChanged();
		super.notifyObservers(this);
		return result;
	}
	
	/**
	 * Set operartion of withdrawal
	 * @param strategy is a type of withdraw
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy=strategy;
	}

	/**
	 * Show detail of purse
	 * @return String of the detail
	 **/
	public String toString() {
		return String.format("%d pieces with value %.1f", this.capacity , getBalance());
	}

}

