package coinpurse;

import java.awt.Font;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class PurseStatus extends JFrame implements Observer{

	JPanel container;
	JLabel resultLabel;
	JProgressBar progressBar;

	public PurseStatus() {
		super.setTitle("Purse Status");
		initComponents();
	}

	private void initComponents() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		container = new JPanel();
		resultLabel = new JLabel("Not Full");
		progressBar = new JProgressBar();
		container.setLayout(new GridLayout(2,1));
		Font font = new Font("Arial",Font.CENTER_BASELINE,28);
		resultLabel.setFont(font);
		container.add(resultLabel);
		container.add(progressBar);
		this.add(container);
		
	}

	public void run() {
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void update(Observable subject, Object message) {
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			progressBar.setMinimum(0);
			progressBar.setMaximum(purse.getCapacity());
			if(purse.isFull()){
			resultLabel.setText("FULL");
			}
			else
			resultLabel.setText(purse.count()+"");
			progressBar.setValue(purse.count());
		}
	}


}
