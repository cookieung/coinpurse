package coinpurse;

/**
 * It's an interface that uses for compare different money.-
 * @author Salilthip 5710546640
 */
public interface Valuable extends Comparable<Valuable>{
	
/**
 * Can get value if implement this interface.
 * @return value that is double
 */
public double getValue( );
}