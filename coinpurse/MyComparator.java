package coinpurse;

import java.util.Comparator;

/**
 * Use for compare value of money
 * @author Salilthip 5710546640
 */
public class MyComparator implements Comparator<Valuable> {
	
	/**
	 * Compare money by Value
	 * @return 1 if value of a more than b
	 * -1 if value of a less than b
	 * or 0 if it doesn't access any case
	 */
	public int compare(Valuable a, Valuable b) {
		if(a.getValue()>b.getValue()) return 1;
		else if(a.getValue()<b.getValue()) return -1;
		else return 0;
	}
}