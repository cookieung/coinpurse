package coinpurse;
import java.util.ArrayList;

import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveStrategy;


/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Salilthip 5710546640
 */
public class Main {

    /**For deposit or withdraw coin
     * @param args not used
     */
    public static void main( String[] args ) {
 
    	Purse purse = new Purse(10);
    	PurseBalance observerBalance = new PurseBalance( );
    	PurseStatus observerStatus = new PurseStatus( );
    	purse.addObserver( observerBalance );
    	purse.addObserver( observerStatus );
    	purse.setWithdrawStrategy(new GreedyWithdraw());
    	ConsoleDialog a = new ConsoleDialog(purse);
    	observerBalance.run();
    	observerStatus.run();
    	a.run();
    	
    }
}
