package coinpurse;

import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class PurseBalance extends JFrame implements Observer {

	JPanel container;
	JLabel resultLabel;

	public PurseBalance() {
		super.setTitle("Purse Balance");
		initComponents();
	}

	private void initComponents() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		container = new JPanel();
		resultLabel = new JLabel("0 Baht");
		this.setLayout(new GridLayout());
		Font font = new Font("Arial",Font.CENTER_BASELINE,28);
		resultLabel.setFont(font);
		container.add(resultLabel);
		this.add(container);
		
	}

	public void run() {
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void update(Observable subject, Object message) {
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			double balance = purse.getBalance();
			resultLabel.setText(balance+"Baht");
		}
	}

}
