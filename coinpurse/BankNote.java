package coinpurse;

/**
 * A BankNote Class.
 * @author Salilthip 5710546640
 */
public class BankNote extends AbstractValuable{

	/**
	 * Value of banknote
	 */
	private double value;
	private String unit;
	
	/**
	 * SerialNumber of BankNote.
	 */
	static long nextSerialNumber=999999;
	
	/**
	 * Set value of BankNote and SerialNumber.
	 * @param value of BankNote.
	 */
	public BankNote(double value , String unit){
		this.value=value;
		this.unit=unit;
		nextSerialNumber++;
	}
	
	/**
	 * Get Value of BankNote.
	 */
	public double getValue(){
		return value;
	}
	
    public String getUnit(){
    	return unit;
    }

	/**
	 * Show detail of BankNote.
	 */
	public String toString(){
		return String.format("%.0f %s", this.getValue(),this.getUnit());
	}
    
	/**
	 * Get SerialNumber.
	 * @return SerialNumber of the BankNote.
	 */
	public static long getNextSerialNumber(){
		return nextSerialNumber;
	}

	
}
