package coinpurse;
import java.util.Map;

import java.util.HashMap;

/**
 * A Coupon Class
 * @author Salilthip 5710546640
 *
 */
public class Coupon extends AbstractValuable{

	/**
	 * Color of Coupons that are different value.
	 */
	private String color;
	/**
	 * Declare map for set value of coupon
	 */
	private static Map<String,Double> map = new HashMap<>();
	
	/**
	 * Set value of each color of coupon
	 */
	static{
		map.put("red",100.0);
		map.put("blue",50.0);
		map.put("green",20.0);
	}
	
	/**
	 * Create a coupon from color.
	 * @param color of the coupon.
	 */
	public Coupon(String color){
		this.color=color;
	}
	
	/**
	 * Get value of coupon.
	 */
	public double getValue(){
		return map.get(color.toLowerCase());
	}
	
    
	/**
	 * Show detail of Coupon.
	 */
	public String toString(){
		return String.format("%.0f Baht", this.getValue());
	}

    
}
