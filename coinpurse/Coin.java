package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Salilthip 5710546640
 */
public class Coin extends AbstractValuable{

    /** Value of the coin */
    private double value;
    private String unit;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin.
     */
    public Coin( double value , String unit) {
        this.value = value;
        this.unit = unit;
    }

    /** 
     * Get value of coin. 
     * @return value of the coin.
     */
    public double getValue(){
    	return value;
    }
    
    public String getUnit(){
    	return unit;
    }


	/**
	 * Show detail of Coin.
	 */
	public String toString(){
		return String.format("%.0f %s", this.getValue(),this.getUnit());
	}

	
    
}