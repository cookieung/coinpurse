package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sun.corba.se.impl.oa.poa.AOMEntry;

import coinpurse.MyComparator;
import coinpurse.Valuable;

/**
 * Find the value that can withdraw
 * @author Salilthip 5710546640
 *
 */
public class RecursiveStrategy implements WithdrawStrategy{

	@Override
	//Comment!!
	public Valuable[] withdraw(double amount, List<Valuable> money) {

	List<Valuable> answer = canWithdraw(amount, money,new ArrayList<Valuable>());
	Valuable[] ans = new Valuable[answer.size()];
	
	answer.toArray(ans);
	
	
	return ans;
	}
	
	/**
	 * Get the array of the number that can withdraw
	 * @param amount is the value that will withdraw
	 * @param money is the pusre that will withdraw
	 * @param list is a simulation for keep the value of money
	 * @return the money that can withdraw
	 */
	
	public List<Valuable> canWithdraw(double amount, List<Valuable> money,List<Valuable> list){

	if(amount==0) return list;
	if(amount<0) return null;
	if(amount>0 && money.size()==0) return null;
	
	list.add(money.get(0));
	List<Valuable> check = canWithdraw(amount-money.get(0).getValue(), money.subList(1, money.size()), list);
	
	if(check==null){
		list.remove(list.get(list.size()-1));
		list = canWithdraw(amount, money.subList(1, money.size()), list);
	}
	return list;
	}

}
