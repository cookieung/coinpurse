package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;

public class GreedyWithdraw implements WithdrawStrategy{

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> money) {
		if (amount <= 0)
			return null;

		Collections.sort(money,new MyComparator());
		Collections.reverse(money);
		ArrayList<Valuable> tempList = new ArrayList<Valuable>();


		for (int i = 0; i < money.size(); i++) {
			if (amount >= money.get(i).getValue()) {
				tempList.add(money.get(i));
				amount -= money.get(i).getValue();
				if (amount == 0) break;
			}
		}

		if (amount != 0)
			return null;
		
		for(int i=0; i<tempList.size() ;i++){
			Valuable c = tempList.get(i);
			money.remove(c);
		}
		
		Valuable[] array = new Valuable[tempList.size()];
		tempList.toArray(array);
		return array;
	}

}
