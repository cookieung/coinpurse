package coinpurse;

import java.util.ResourceBundle;

/**
 * Encapsulate operation of creating money objects.
 */
public abstract class MoneyFactory {
	
	private static MoneyFactory instance = null;
    
    protected MoneyFactory() {
   
    }
    
    /** Get the money factory instance. */
    public static MoneyFactory getInstance() {
    	if(instance==null){
    		ResourceBundle bundle = ResourceBundle.getBundle( "coinpurse.purse" );
    		String value = bundle.getString( "moneyfactory" );
    		
    		
    		if(value!=null){
    		System.out.println("Factory class is " + value);  //  testing
    		try {
    		    instance = (MoneyFactory)Class.forName(value).newInstance();
    		} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException ex) {
    		//TODO what exceptions might be thrown?
    		    System.out.println("Error creating MoneyFactory "+ex.getMessage() );
    		    instance = new ThaiMoneyFactory();
    		}
    		}
    		else{
    			instance = new ThaiMoneyFactory();
    		}
    	}
    	//instance = new ThaiMoneyFactory();
    	return instance;
    }
    
    abstract Valuable createMoney(double value);
    
    Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));
	}

    public static void setMoneyFactory(MoneyFactory mf){
    	instance= mf;
    }
	
}
